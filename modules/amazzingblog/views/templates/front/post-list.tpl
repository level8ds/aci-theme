{*
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*
*}

{if $posts}
	{if !empty($ab_pagination_settings) && empty($no_pagination)}
		{include file = $blog->getTemplatePath('pagination.tpl') settings = $ab_pagination_settings p_type = $settings.p_type}
	{/if}
    {$col = false}{if $settings.display_type == 'grid'}{$col = 12/$settings.col_num}{/if}
	{if empty($settings.compact)}{$item_tpl = 'post-list-item.tpl'}{else}{$item_tpl = 'post-list-item-compact.tpl'}{/if}
	{$item_tpl_path = $blog->getTemplatePath($item_tpl)}
	{$tags_tpl_path = $blog->getTemplatePath('post-tags.tpl')}
	{if !empty($cms.id) && $cms.id == 4}

	<div class="post-list item-list {$settings.display_type|escape:'html':'UTF-8'}{if $col} row{/if}"><h2 class="liner">Our Team</h2></div>
	<div class="post-list item-list grid row"><h3 class="office">Auburn Office</h3></div>
	
	
	{foreach $posts as $k => $post}
	{if $post.id_category_default == 6}
	{assign var='asales' value=$asales+1}
	{if $asales == 1}
	<div class="post-list item-list grid row"><h4>Sales Team</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

	
	{foreach $posts as $k => $post}
	{if $post.id_category_default == 13}
	{assign var='astep' value=$astep+1}
	{if $astep == 1}
	<div class="post-list item-list grid row"><h4>Step and Rail Sales</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}


	{foreach $posts as $k => $post}
	{if $post.id_category_default == 7}
    {assign var='aadmin' value=$aadmin+1}
	{if $aadmin == 1}
		<div class="post-list item-list grid row"><h4>Administrative Team</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

{foreach $posts as $k => $post}
	{if $post.id_category_default == 18}
	 {assign var='aadmina' value=$aadmina+1}
	{if $aadmina == 1}
		<div class="post-list item-list grid row"><h4>Administrative Support</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

	<div class="post-list item-list grid row"><h3 class="office">Bangor Office</h3></div>
    
	{foreach $posts as $k => $post}
	{if $post.id_category_default == 9}
	{assign var='bsales' value=$bsales+1}
	{if $bsales == 1}
	<div class="post-list item-list grid row"><h4>Sales Team</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

	{foreach $posts as $k => $post}
	{if $post.id_category_default == 12}
	{assign var='bstep' value=$bstep+1}
	{if $bstep == 1}
	<div class="post-list item-list grid row"><h4>Step and Rail Sales</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

	{foreach $posts as $k => $post}
	{if $post.id_category_default == 10}
	 {assign var='badmin' value=$badmin+1}
	{if $badmin == 1}
		<div class="post-list item-list grid row"><h4>Administrative Team</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

	{foreach $posts as $k => $post}
	{if $post.id_category_default == 19}
	 {assign var='badmins' value=$badmins+1}
	{if $badmins == 1}
		<div class="post-list item-list grid row"><h4>Administrative Support</h4></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

	{foreach $posts as $k => $post}
	{if $post.id_category_default == 15}
	{assign var='servicet' value=$servicet+1}
	{if $servicet == 1}
 <div class="post-list item-list grid row"><h3 class="office">Service Team</h3></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}

    

	{foreach $posts as $k => $post}
	{if $post.id_category_default == 16}
	{assign var='agr' value=$agr+1}
	{if $servicet == 1}
	<div class="post-list item-list grid row"><h3 class="office">Agricultural Sales Team</h3></div>
	{/if}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}


	<div class="post-list item-list grid row"></div>

{elseif !empty($cms.id) && $cms.id == 7}
	{foreach $posts as $k => $post}
	{if $post.id_category_default == 17}
	{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/if}
	{/foreach}</div>
<div class="post-list item-list grid row"></div>
{else}
	<div class="post-list item-list {$settings.display_type|escape:'html':'UTF-8'}{if $col} row{/if}">
	{foreach $posts as $k => $post}
		{include file = $item_tpl_path post = $post col = $col tags_tpl_path = $tags_tpl_path}
	{/foreach}</div>
	{/if}
{else}
	<div class="alert-warning">{l s='No posts' mod='amazzingblog'}</div>

{/if}

{* since 1.3.0 *}

