{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-9999 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}
</div>
<div class="row tertiary clearfix">
    <div class="col-lg-4 col-md-6">
         <a class="btn btn-secondary" href="" data-toggle="modal" data-target=".quote"><i class="material-icons"></i> Request a Quote </a>
         <a class="btn btn-secondary hidden" href="{$urls.base_url}installer"><i class="material-icons"></i>Find an Installer </a>
     </div>
     <div class="col-lg-2 col-md-3">
         <h5>Auburn</h5>
         <span>(207) 784-1388 </span>
     </div>
     <div class="col-lg-2 col-md-3">
         <h5>Bangor</h5>
         <span>(207) 947-8334</span>
     </div>
     <div class="col-md-4 contact-ft">
        <h4><i class="material-icons"></i> CONTACT US FOR</h4>
        <h2>QUOTES &amp; ORDERS</h2>
     </div>
</div>
    <div id="related-products" class="clearfix">
        <h3 class="liner">Related Products</h3>
        <div class="row">
{foreach from=$blocksProductFooter item=block}
    <div id="ppbContainer{$block->id|escape:'int':'utf-8'}" class="featured-products block products_block clearfix {if $block->carousell==1}ppbContainerBlockMainDiv ppbCarouselBlock{/if}">
        <h1 class="h1 products-section-title text-uppercase ">{$block->name|escape:'html':'utf-8'}{if $block->carousell_controls==1 && $block->carousell==1}<i class="ppbback ppbb{$block->id} material-icons">&#xE5CB;</i><i class="material-icons ppbf{$block->id} ppbforward">&#xE5CC;</i>{/if}</h1>
        {if isset($block->products) && $block->products}
            <div class="ppb{$block->id} featured-products" id="ppb{$block->id}">
                <div class="products">
                    {foreach from=$block->products item="product"}
                        {if $block->carousell==1}
                            <div class="col-lg-4 col-md-3 col-sm-2 col-xs-1">
                        {/if}
                        {include file="catalog/_partials/miniatures/related.tpl" product=$product}
                        {if $block->carousell==1}
                            </div>
                        {/if}
                    {/foreach}
                </div>
            </div>
        {else}
            <ul class="ppbContainer{$block->id|escape:'int':'utf-8'}_noProducts tab-pane">
                <li class="alert alert-info">{l s='No products at this time.' d='Module.Ppb.Shop'}</li>
            </ul>
        {/if}
    </div>
    {if $block->carousell==1}
    {literal}
        <script>
            document.addEventListener("DOMContentLoaded", function(event) {
                var {/literal}ppb{$block->id}{literal} = $('.{/literal}ppb{$block->id}{literal} .products').lightSlider(
                        {
                            item: {/literal}{$block->carousell_nb}{literal},
                            loop: false,
                            slideMove: 1,
                            slideMargin: 0,
                            speed: 600,
                            pager: {/literal}{if $block->carousell_pager==1}true{else}false{/if}{literal},
                            loop: {/literal}{if $block->carousell_loop==1}true{else}false{/if}{literal},
                            controls: false,
                            pauseOnHover: true,
                            auto: {/literal}{if $block->carousell_auto==1}true{else}false{/if}{literal},
                            responsive: [
                                {
                                    breakpoint: 800,
                                    settings: {
                                        item: {/literal}{$block->carousell_nb_tablet}{literal},
                                        slideMove: 1,
                                        slideMargin: 0,
                                    }
                                },
                                {
                                    breakpoint: 480,
                                    settings: {
                                        item: {/literal}{$block->carousell_nb_mobile}{literal},
                                        slideMove: 1,
                                        slideMargin: 0
                                    }
                                }
                            ]
                        }
                );
                {/literal}
                {if $block->carousell_controls==1 && $block->carousell==1}
                {literal}
                $('.ppbb{/literal}{$block->id}{literal}').click(function () {
                    {/literal}ppb{$block->id}{literal}.goToPrevSlide();
                });
                $('.ppbf{/literal}{$block->id}{literal}').click(function () {
                    {/literal}ppb{$block->id}{literal}.goToNextSlide();
                });
                {/literal}
                {/if}
                {literal}

            });
        </script>
    {/literal}
    {/if}
{/foreach}
</div>
</div>
