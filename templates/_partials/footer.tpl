     {if $page.meta.title == 'About us'}
     <div class="concrete">
     {hook h='displayBlog2'}
   </div>
     {/if}
     {if $page.meta.title == 'Services'}
     <div class="concrete">
     {hook h='displayBlog2'}
   </div>
     {/if}
<div class="container">
    {if $smarty.server.REQUEST_URI == '/' }
    {elseif $page.meta.title == 'Contact us'}
    {elseif empty($id_category_current)}
    <div class="row tertiary clearfix">
        <div class="col-lg-4 col-md-6">
             <a class="btn btn-secondary" href="" data-toggle="modal" data-target=".quote"><i class="material-icons"></i> Contact Us </a>
         </div>
         <div class="col-lg-2 col-md-3">
             <h5>Auburn</h5>
             <span>(207) 784-1388 </span>
         </div>
         <div class="col-lg-2 col-md-3">
             <h5>Bangor</h5>
             <span>(207) 947-8334</span>
         </div>
         <div class="col-md-4 contact-ft">
            <h4><i class="material-icons"></i> CONTACT US FOR</h4>
            <h2>QUOTES &amp; ORDERS</h2>
         </div>
    </div>
    <div class="modal fade quote" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">×</span>
             </button>
           <div class="modal-header">
            
             <h5 class="liner">Contact Us</h5>
           </div>
           <div class="modal-body">
       {hook h='displayGform' id='2'}
           </div>
         </div>
     </div>
    </div>
    {/if}
  <div class="row pre">
    {hook h='displayFooterBefore'}
  </div>

<div class="footer-container">
  <div class="container">
    <div class="row mm hidden-sm-down">
        <nav>
          <a href="{$urls.pages.contact}">{l s='Contact us' d='Shop.Theme'}</a>
          <a href="{$urls.base_url}news-media/" >News & Media</a>
          <a href="{$link->getCMSLink('4', 'about-us')|escape:'html'}">About Us</a>
         <a href="{$link->getCMSLink('7', 'services')|escape:'html'}" >Services</a>
         <a href="{$link->getCMSLink('6', 'products')|escape:'html'}" >Products</a>
         </nav>
         <div style="clear:both;"></div>
      {hook h='displayFooter'}
    </div>
    <div class="row">
      {hook h='displayFooterAfter'}
    </div>
    <div class="row bottom">
        <div class="cat-menu nav">
        <nav class="categories hidden-md-up">
    <ul>
        <li><a href="{$urls.base_url}">Home</a></li>
        <li ><a href="{$link->getCMSLink('6', 'products')|escape:'html'}" >Products</a></li>
<li ><a href="{$link->getCMSLink('7', 'services')|escape:'html'}" >Services</a></li>
<li ><a href="{$link->getCMSLink('4', 'about-us')|escape:'html'}">About Us</a></li>
<li ><a href="{$urls.base_url}news-media/" >News & Media</a></li>
<li ><a href="{$urls.pages.contact}">{l s='Contact us' d='Shop.Theme'}</a></li>
    
    </ul>
</nav>
</div><br/>
      <div class="col-md-12">
        <p>
            &copy; {'Y'|date} All rights reserved. <br/>
          <a class="_blank" href="//level8ds.com" target="_blank">
        website design: level8 design studio
          </a>
        </p>
      </div>
    </div>
  </div>
</div>
</div>
