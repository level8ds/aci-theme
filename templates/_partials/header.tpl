{block name='header_top'}
  <div class="header-top hidden-sm-down">
    <div class="container">        
       <div class="row">
        <div class="col-md-3" >
          <a href="{$urls.base_url}">
            <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
          </a>
        </div>
          <dl class="row">
              <dd class="col-xs-2">Auburn (207) 784-1388</dd>
              <dd class="col-xs-2">Bangor (207) 947-8334</dd>
            <dd class="col-xs-4">{hook h='displayTop'}</dd></dd>
        </dl>
      </div>
      <div class="row mm">
          <nav>
            <a href="{$urls.pages.contact}">{l s='Contact us' d='Shop.Theme'}</a>
            <a href="{$urls.base_url}news-media/">News & Media</a>
            <a href="{$link->getCMSLink('4', 'about-us')|escape:'html'}">About Us</a>
            <a href="{$link->getCMSLink('7', 'services')|escape:'html'}" >Services</a>
            <a href="{$link->getCMSLink('6', 'products')|escape:'html'}" >Products</a>
           </nav>
      </div>
       <div style="clear:both;"></div>
       
    </div>
  </div>

  <div class="header-top row hidden-md-up">
           <div class="col-xs-6 col-sm-5" >
             <a href="{$urls.base_url}">
               <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
             </a>
           </div>
           <div class="offset-xs-2 col-xs-4 offset-sm-4 col-sm-3" >
            <a id="msearch"><i  class="material-icons tertiary">&#xE8B6;</i></a>
             <a id="mmenu"><i  class="material-icons primary">&#xE3C7;</i></a>
             <a href="" data-toggle="modal" data-target=".phone"><i  class="material-icons secondary">&#xE0CD;</i></a>
         </div>
      </div>
      <div class="modal fade phone" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h5 class="liner">Call Us</h5>
        </div>
        <div class="modal-body">
            <p><strong>AUBURN</strong> <a href="2077841388"> (207) 784-1388</a></p>
            <p><strong>BANGOR</strong> <a href="2077841388"> (207) 947-8334</a></p>
        </div>
      </div>
  </div>
</div>
{/block}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129495661-1"></script>
<div id="mobilesearch" class="tertiary row hidden-md-up">
{hook h='displayTop'}
</div>
<div id="mobilemenu" class="primary hidden-md-up">
        <ul class="list-group primary ">
          <li class="list-group-item"><a href="{$link->getCMSLink('6', 'products')|escape:'html'}" >Products</a></li>
          <li class="list-group-item"><a href="{$link->getCMSLink('7', 'services')|escape:'html'}" >Services</a></li>
          <li class="list-group-item"><a href="{$link->getCMSLink('4', 'about-us')|escape:'html'}">About Us</a></li>
          <li class="list-group-item"><a href="{$urls.base_url}news-media/" >News & Media</a></li>
          <li class="list-group-item"><a href="{$urls.pages.contact}">{l s='Contact us' d='Shop.Theme'}</a></li>
        </ul>
</div>
<div class="end hidden-md-up"><a id="mcats"><i class="material-icons">&#xE148;</i> Product Categories</a></div>
