{block name='product_miniature_item'}
<article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
    
<div class="card clearfix">
    <div class="card-block">
    
        <div class="col-md-3 first">
            {block name='product_thumbnail'}
            <a href="{$product.url}" class="thumbnail product-thumbnail">
            <img src = "{$product.cover.bySize.home_default.url}"
            alt = "{$product.cover.legend}"
            data-full-size-image-url = "{$product.cover.large.url}">
            </a>
            {/block}
        </div>
        
      {block name='product_name'}
      <div class="phead col-md-9">
        <h2  itemprop="name"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h2> <span>Datasheets
        {foreach from=$product.attachments item=downloads}
        {if $downloads.mime == "application/pdf"}<a href="{$urls.base_url}index.php?controller=attachment&id_attachment={$downloads.id_attachment}" class="pdf"><i class="material-icons large">&#xE24D;</i></a>
        {elseif $downloads.mime == "application/dwg"}
        <a href="{$urls.base_url}index.php?controller=attachment&id_attachment={$downloads.id_attachment}" class="dwg"><i class="material-icons">&#xE55B;</i></a>
        {/if}
        {/foreach}</span>
       </div>
      {/block}
    <div class="lists col-md-3">
        <ul class="plist first">
            <li><span class="name">Category:</span><span class="value">{$product.category_name }</span></li>
            {foreach from=$product.attributes_comma_lists key=key  item=at}
            <li><span class="name">{$key}:</span><span class="value">{$at}</span> </li>
            {/foreach}
        </ul>
    </div>
    <div class="lists col-md-5">
        <ul class="plist second">
            {foreach from=$product.features item=feature}
            <li><span class="name">{$feature.name}:</span><span class="value">{$feature.value}</span> </li>
            {/foreach}
        </ul>
    </div>

    </div>
</div>
</article>
{/block}
