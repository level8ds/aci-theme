


        <div class="col product-block">
            {block name='product_thumbnail'}
            <a href="{$product.url}" class="thumbnail product-thumbnail">
            <img src = "{$product.cover.bySize.home_default.url}"
            alt = "{$product.cover.legend}"
            data-full-size-image-url = "{$product.cover.large.url}">
            {/block}
            {block name='product_name'}
             <span>{$product.name}</span></a>
            {/block}
        </div>
