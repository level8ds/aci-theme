<div class="product-variants">
    <p>{$product.description nofilter}</p>
    <h5>Specs</h5>
  <ul class="plist">
      <li><span>Category: {$product.category_name}</span></li>
  {foreach from=$groups key=id_attribute_group item=group}
      {if $group.group_type == 'radio'}
          <li><span>{$group.name}:{counter start=0 print=false}{foreach from=$group.attributes key=id_attribute item=group_attribute}
            {foreach $group_attribute.name item=specs}{counter assign=specnum}{$specs}{if $specnum < $group.attributes_quantity|@count },{/if}{/foreach}
          {/foreach}</span></li>
      {/if}
  {/foreach}
  {foreach from=$product.features item=feature}
  <li><span>{$feature.name}: {$feature.value}</span> </li>
  {/foreach}
    </ul>
    {if !empty($product.attachments)}
    <strong class="clearfix">Downloads</strong><br/>
    {foreach from=$product.attachments item=downloads}
    {if $downloads.mime == "application/pdf"}<a href="{$urls.base_url}index.php?controller=attachment&id_attachment={$downloads.id_attachment}" target="_blank" class="pdf"><i class="material-icons large">&#xE24D;</i><span>{$downloads.name}</span></a>
    {elseif $downloads.mime == "application/dwg"}
    <a href="{$urls.base_url}index.php?controller=attachment&id_attachment={$downloads.id_attachment}" target="_blank" class="dwg"><i class="material-icons">&#xE55B;</i><span>{$downloads.name}</span></a>
    {/if}
    {/foreach}
    {/if}
</div>
