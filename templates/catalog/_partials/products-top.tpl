
<div id="js-product-list-top" class="row products-selection">  
  <div class="col-md-6 hidden-sm-down total-products">
    {if $listing.products|count > 1}
      <p> {l s='Showing %from%-%to% of %total% item(s)' d='Shop.Theme.Catalog' sprintf=[
    '%from%' => $listing.pagination.items_shown_from ,
    '%to%' => $listing.pagination.items_shown_to,
    '%total%' => $listing.pagination.total_items
    ]}
  </p>
    {else}
      <p>{l s='There is %product_count% product.' d='Shop.Theme.Catalog' sprintf=['%product_count%' => $listing.products|count]}</p>
    {/if}
  </div>
  <div class="col-md-6">
    <div class="row">


      {if !empty($listing.rendered_facets)}
        <div class="col-sm-12 col-xs-12 hidden-md-up filter-button">
          <button id="search_filter_toggler" class="btn btn-secondary">
            {l s='Product Search Filters' d='Shop.Theme.Actions'}
          </button>
        </div>
      {/if}
    </div>
  </div>
  <div class="col-sm-12 hidden-md-up text-xs-center showing">
    {l s='Showing %from%-%to% of %total% item(s)' d='Shop.Theme.Catalog' sprintf=[
    '%from%' => $listing.pagination.items_shown_from ,
    '%to%' => $listing.pagination.items_shown_to,
    '%total%' => $listing.pagination.total_items
    ]}
  </div>
</div>
{if $activeFilters|count}
  <section id="js-active-search-filters" class="active_filters">
    <h1 class="h6 active-filter-title">{l s='Active filters' d='Shop.Theme'}</h1>
    <ul>
      {foreach from=$activeFilters item="filter"}
        <li class="filter-block">{l s='%1$s: ' d='Shop.Theme.Catalog' sprintf=[$filter.facetLabel]} {$filter.label} <a class="js-search-link" href="{$filter.nextEncodedFacetsURL}"><i class="material-icons close">&#xE5CD;</i></a></li>
      {/foreach}
    </ul>
  </section>
{/if}