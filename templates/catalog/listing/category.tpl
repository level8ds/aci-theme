{extends file='catalog/listing/product-list.tpl'}
{block name='product_list_header'}
      <h1 class="liner">{$category.name}</h1>
        {if $listing.products|count > 1}
<div class="prodalert">
<p>We are in the process of adding our products to the site. If you don't see what you are looking for <a href="contact-us">Please Contact Us</a> for more information.</p></div>{/if}
      {if $category.description}
        <div id="category-description" class="text-muted">{$category.description nofilter}</div>
        <div class="category-cover">
          <img src="{$category.image.large.url}" alt="{$category.image.legend}">
        </div>
      {/if}
    <div class="text-xs-center hidden-md-up">
      <h1 class="h1">{$category.name}</h1>
    </div>
{/block}
