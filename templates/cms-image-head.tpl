{if $page.meta.title == 'About us'}
<div id="carousel" data-ride="carousel" class="carousel slide" data-interval="5000" data-wrap="true" data-pause="hover">
    <ul class="carousel-inner" role="listbox">
              <li class="carousel-item active">
          <figure>
            <img src="/img/about-aci.jpg">
                          <figcaption class="caption">
                <div class="caption-description"><h2>WE KNOW CONCRETE</h2></div>
                <h2 class="display-1 text-uppercase">Serving SINCE 1957</h2>
              </figcaption>
                      </figure>
        </li>
          </ul>

  </div>
{elseif $page.meta.title == 'Products'}
  <div id="carousel" data-ride="carousel" class="carousel slide" data-interval="5000" data-wrap="true" data-pause="hover">
      <ul class="carousel-inner" role="listbox">
                <li class="carousel-item active">
            <figure>
              <img src="/img/products.jpg">
                            <figcaption class="caption">
                  <div class="caption-description"><h2>SMALL TO MASSIVE</h2></div>
                  <h2 class="display-1 text-uppercase">Great Products</h2>
                </figcaption>
                        </figure>
          </li>
            </ul>

    </div>
{elseif $page.meta.title == 'Services' }
<div id="carousel" data-ride="carousel" class="carousel slide" data-interval="5000" data-wrap="true" data-pause="hover">
    <ul class="carousel-inner" role="listbox">
              <li class="carousel-item active">
          <figure>
            <img src="/img/services.jpg">
                          <figcaption class="caption">
                <div class="caption-description"><h2>SUPPORT YOUR INFRASTRUCTURE</h2></div>
                <h2 class="display-1 text-uppercase">With Concrete services</h2>
              </figcaption>
                      </figure>
        </li>
          </ul>

  </div>
  {else}

{/if}
