{extends file='page.tpl'}

{block name='page_title'}
  {$cms.meta_title}
{/block}

{block name='page_content_container'}
  {if !empty($cms.id) && $cms.id == 4 }
  {$cms.content nofilter}
    {hook h='displayCMSDisputeInformation'}
      {hook h='displayCMSPrintButton'}
  {elseif !empty($cms.id) && $cms.id == 7 }
  {$cms.content nofilter}
    {hook h='displayCMSDisputeInformation'}
      {hook h='displayCMSPrintButton'}
  {else}
  <section id="content" class="page-content page-cms page-cms-{$cms.id} clearfix">
    {$cms.content nofilter}
    
    {hook h='displayCMSDisputeInformation'}
    
    {hook h='displayCMSPrintButton'}
  </section>
  {/if}
{/block}
