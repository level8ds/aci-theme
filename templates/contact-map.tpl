<div class="concrete map">
<h3 class="liner">Our Locations</h3>
<div class="row">
  <div class="col-sm-6">
    <div class="card">
        <dl class="row card-block">
            <dt class="col-sm-4"><h5>Auburn</h5></dt>
            <dd class="col-sm-3">P (207) 784-1388</dd>
            <dd class="col-sm-1"><i class="material-icons">&#xE047;</i></dd>
            <dd class="col-sm-3"><span>M-F 6am to 5pm</span></dd>
        </dd>
    </dl>
        <div class="google-maps">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2866.2123853844937!2d-70.27227558448838!3d44.07896997910894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cb26996f24014db%3A0xb0c405a1ebc40d8b!2sSuperior+Concrete!5e0!3m2!1sen!2sus!4v1543333495999" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    </div>
    
    <div class="col-sm-6">
    <div class="card">
            <dl class="row card-block">
                <dt class="col-sm-4"><h5>Bangor</h5></dt>
                <dd class="col-sm-3">P (207) 947-8334</dd>
                <dd class="col-sm-1"><i class="material-icons">&#xE047;</i></dd>
                <dd class="col-sm-3"><span>M-F 6am to 5pm</span></dd>
            </dd>
        </dl>
        <div class="google-maps">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5656.107455413098!2d-68.72235!3d44.861201!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cafb472a5ce693d%3A0x2b64b4ce9f707b3d!2sAmerican+Concrete+Industries!5e0!3m2!1sen!2sus!4v1487635298585" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    </div>
  
</div>
</div>
