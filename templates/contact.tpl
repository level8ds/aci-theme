{extends file='page.tpl'}

{block name='page_header_container'}{/block}

{block name='left_column'}
  <div id="left-column" class="col-xs-12 col-sm-3">
    {widget name="ps_contactinfo" hook='displayLeftColumn'}
  </div>
{/block}

{block name='page_content'}
<div class="concrete map">
<h3 class="liner">Contact Us</h3>
<div class="row">
  <div class="col-sm-12">
      <div class="card">
  <div class="card-header">
    <h5>Auburn <a class="btn btn-primary" href="https://www.google.com/maps/dir//superior+concrete+auburn+maine/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x4cb26996f24014db:0xb0c405a1ebc40d8b?ved=2ahUKEwizu9bI9vTeAhUv11kKHV2ICtQQ9RcwC3oECAMQDg" target="_blank"><i class="material-icons"></i>Get Directions</a></h5>
  </div>
  <div class="card-block">
      <dl class="row card-block">
          <dt class="col-sm-4">
              <ul>
                  <li><br/></li>
                  <li><strong>M-F 6am to 5pm</strong></li>
              </ul>
              <ul>
                  <li>982 Minot Ave.</li>
                  <li>Auburn, ME 04210 USA</li>
              </ul>
              <ul>
                  <li><strong>P</strong> (207)-784-1388</li>
                  <li><strong>F</strong> (207)-783-4039</li>
              </ul>
              <ul>
                  <li><span>&nbsp;</span></li>
                  <li><span>&nbsp;</span></li>
              </ul>
          </dt>
          <dd class="col-sm-8"><div class="google-maps">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2866.2123853844937!2d-70.27227558448838!3d44.07896997910894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cb26996f24014db%3A0xb0c405a1ebc40d8b!2sSuperior+Concrete!5e0!3m2!1sen!2sus!4v1543333495999" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div></dd>
          
  </dl>

  </div>
</div>
</div>
<div class="col-sm-12">
    <div class="card">
<div class="card-header">
  <h5>Bangor<a class="btn btn-primary" href="https://www.google.com/maps/place/American+Concrete+Industries/@44.861201,-68.72235,16z/data=!4m15!1m9!4m8!1m3!2m2!1d-68.7212342!2d44.8611934!1m3!2m2!1d-68.7223503!2d44.8612013!3m4!1s0x4cafb472a5ce693d:0x2b64b4ce9f707b3d!8m2!3d44.8612073!4d-68.7223502?hl=en-US" target="_blank"><i class="material-icons"></i>Get Directions</a></h5>
</div>
<div class="card-block">
    <dl class="row card-block">
        <dt class="col-sm-4">
            <ul>
                <li><br/></li>
                <li><strong>M-F 6am to 5pm</strong></li>
            </ul>
            <ul>
                <li>1717 Stillwater Ave.</li>
                <li>Veazie, ME 04401 USA</li>
            </ul>
            <ul>
                <li><strong>P</strong> (207)-947-8334</li>
                <li><strong>F</strong> (207)-947-3580</li>
            </ul>
            <ul>
                <li><span>&nbsp;</span></li>
                <li><span>&nbsp;</span></li>
            </ul>
        </dt>
        <dd class="col-sm-8"><div class="google-maps">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5656.107455413098!2d-68.72235!3d44.861201!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cafb472a5ce693d%3A0x2b64b4ce9f707b3d!2sAmerican+Concrete+Industries!5e0!3m2!1sen!2sus!4v1487635298585" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div></dd>
        
</dl>

</div>
</div>
    
    </div>
    
    
  
</div>
</div>
<div id="custom-text">
    <div class="spaced">
    <h2>Send Us a Message</h2>
{hook h='displayGform' id='2'}
</div>
</div>
{/block}
