<section id="content" class="page-content page-not-found">
  {if $listing.products|count < 1 && !empty($listing) && $page.meta.title != 'Search'}
   <h4>Please Contact Us</h4>
   <p>We are in the process of adding our products to the website. In the meantime, please <a href="contact-us">Contact Us</a> to discuss how we can help.<br/><a class="btn btn-secondary tertiary" href="contact-us" ><i class="material-icons"></i> Contact Us </a></p>
{elseif $page.meta.title == 'Search' }
<h4>Uhoh! Sorry!</h4>
<p>Your search for <strong>{$search_string}</strong> returned no results. Please try again or <a href="contact-us">Contact Us</a> to discuss how we can help.<br/><a class="btn btn-secondary tertiary" href="contact-us" ><i class="material-icons"></i> Contact Us </a></p>
 {else}
  <h4>Sorry For the Inconvience</h4>
   <p>Looks like you did not find what you are looking for. Please  <a href="contact-us">Contact Us</a> to discuss how we can help.<br/><a class="btn btn-secondary tertiary" href="contact-us" ><i class="material-icons"></i> Contact Us </a></p>
  {/if}
</section>
