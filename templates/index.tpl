{extends file='page.tpl'}

    {block name='page_content_container'}
    {include file="cms-image-head.tpl"}
      <section id="content" class="page-home">
        {block name='page_content_top'}{/block}
        {block name='page_content'}
          {$HOOK_HOME nofilter}
          {include file="contact-map.tpl"}
        {/block}
      </section>
    {/block}
