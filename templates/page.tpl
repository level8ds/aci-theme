{extends file=$layout}
{block name='content'}
{if $page.meta.title == 'About us'|| $page.meta.title == 'Products'|| $page.meta.title == 'Services' }
{include file="cms-image-head.tpl"}
{/if}
{if $page.meta.title != 'Products' }
 <section id="main" {if $page.meta.title == 'About us'|| $page.meta.title == 'Products'|| $page.meta.title == 'Services'  || $smarty.server.REQUEST_URI == '/news-media/' ||  $page.meta.title == '404 error' }class="concrete"{/if}>
    {block name='page_header_container'}
      {block name='page_title' hide}
        <header class="page-header">
          <h1 class="liner">{$smarty.block.child}</h1>
      </header>
        
      {/block}
    {/block}
    {block name='page_content_container'}
      <section id="content" class="page-content card card-block">
        {block name='page_content_top'}{/block}
        {block name='page_content'}
          <!-- Page content -->
        {/block}
        
      </section>
      <div class="album text-muted">
      <div class="container">

      </div>
    </div>
    {/block}
    
    {block name='page_footer_container'}
      <footer class="page-footer">
        {block name='page_footer'}
          <!-- Footer content -->
        {/block}
      </footer>
    {/block}

  </section>
  {else}
  {hook h='displaycat'}
  {/if}
{/block}
